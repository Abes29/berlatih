<?php
    require_once("animal.php");
    require_once("Frog.php");
    require_once("ape.php");
    
    
    $Sheep= new Animal("Shaun");
    print "Name : $Sheep->name <br>";
    print "Legs : $Sheep->legs <br>";
    print "cold_blooded : $Sheep->cold_blooded <br> <br>";

    $kodok = new Frog("buduk");
    echo "Name : . $kodok->name <br>";
    echo "Legs : . $kodok->legs <br>";
    echo "cold_blooded : . $kodok->cold_blooded <br>";
    $kodok->Jump();
    echo "<br> <br>";

    $sungokong = new Ape("kera sakti");
    print "Name : $sungokong->name <br>";
    print "Legs : $sungokong->legs <br>";
    print "cold_blooded : $sungokong->cold_blooded<br>";
    $sungokong->yell();
?>